import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
part 'splash_screen_event.dart';
part 'splash_screen_state.dart';

class SplashScreenBloc extends Bloc<SplashScreenEvent, SplashScreenState>{

  @override
  SplashScreenState get initialState => Initial();

  @override
  Stream<SplashScreenState> mapEventToState(SplashScreenEvent event) async*{
    if(event is NavigateToHomeScreenEvent){
      yield Loading();

      await Future.delayed(Duration(seconds: 3));

      yield Loaded();
    }
  }
}