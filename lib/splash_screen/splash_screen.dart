import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../firebase/user_repository.dart';
import '../login/login_page.dart';
import 'bloc/splash_screen_bloc.dart';
import 'splash_screen_widget.dart';

class SplashScreen extends StatelessWidget {
  final UserRepository _userRepository;

  SplashScreen({Key key, @required UserRepository userRepository})
      :assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
    );
  }

  BlocProvider<SplashScreenBloc> _buildBody(BuildContext context) {
    return BlocProvider(
      create: (context) => SplashScreenBloc(),
      child: Container(
        height: MediaQuery
            .of(context)
            .size
            .height,
        width: MediaQuery
            .of(context)
            .size
            .width,
        color: Colors.white,
        child: Center(
          child: BlocBuilder<SplashScreenBloc, SplashScreenState>(
              builder: (context, state) {
                if ((state is Initial) || (state is Loading)) {
                  print('SplashScreen show state $state');
                  return SplashScreenWidget();
                } else {
                  print('SplashScreen show state $state');
                  return LoginPage(userRepository: _userRepository);
                }
              }
          ),
        ),
      ),
    );
  }
}