import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../authentication_bloc/authentication_bloc.dart';
import '../authentication_bloc/bloc.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Text('This is HomePage')),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () =>
            BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut()),
        label: Text('Logout'),
        icon: Icon(Icons.arrow_back),
      ),
    );
  }
}
