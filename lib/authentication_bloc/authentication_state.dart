import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
@immutable
abstract class AuthenticationState extends Equatable {
  AuthenticationState([List props = const []]) : super();
}

class Uninitialized extends AuthenticationState {
  @override
  String toString() {
    return 'Uninitialized';
  }

  @override
  List<Object> get props => throw UnimplementedError();
}

class Authenticated extends AuthenticationState {
  final String userName;

  Authenticated(this.userName) : super([userName]);

  @override
  String toString() {
    return 'Authenticated {UserName: $userName}';
  }

  @override
  List<Object> get props => throw UnimplementedError();
}

class Unauthenticated extends AuthenticationState {
  @override
  String toString() {
    return 'Unauthenticated';
  }

  @override
  List<Object> get props => throw UnimplementedError();
}
